import React from "react";
import "./style.css";
import externBubbleSort from "../AlgosTris/BubbleSort";
import {
  externInsertionSort,
  getOperationTypeInsertionSort,
} from "../AlgosTris/InsertionSort";
import {
  externQuickSort,
  getOperationTypeQuickSort,
} from "../AlgosTris/QuickSort";
import { externMergeSort } from "../AlgosTris/MergeSort";
import { externHeapSort, 
  getOperationTypeHeapSort, 
          getOperationTypeMergeSort
} from "../AlgosTris/HeapSort";
import {
  getRndInteger,
  arraysAreEquals,
  clearAllTimeOut,
  startTimer,
} from "../tools/tools";

/* Constantes au lancement de l'application */
const NB_VALUES_PETIT = 10;
const NB_VALUES_MOYEN = 100;
const NB_VALUES_GRAND = 300;
const VALUE_WIDTH = window.innerWidth / (2 * NB_VALUES_MOYEN);

/* La vitesse d'animation au démarrage*/
const ANIM_SPEED_PETIT =  300;
const ANIM_SPEED_MOYEN = 8;
const ANIM_SPEED_GRAND = 1;

/* Constantes pour les types d'opération à effectuer*/
const COMP = 0;
const COMP_GAUCHE = -1;
const COMP_DROITE = 0;
const SWAP = 1;
const TO_INSERT = 2;
const PIVOT = 2;

/* Les couleurs lors des différentes étapes de l'algo */
const COLOR_COMP = "#a32933";
const COLOR_SWAP = "green";
const COLOR_DEFAULT = "turquoise";
const COLOR_INSERT = "red";
const COLOR_SORTED = "#fccc0d";
const COLOR_PIVOT = "yellow";

class SortingVisualizer extends React.Component {
  state = {
    values: [],
    styleButtonSort: "btn btn-info m-2 buttonSort",
    styleButtonSize: "btn btn-outline-info m-2 buttonSize",
    styleButtonNewArray: "btn btn-outline-warning m-2",
    nb_values: NB_VALUES_MOYEN,
    value_width: VALUE_WIDTH,
    anim_speed: ANIM_SPEED_MOYEN,
  };

  /*
    Désactive les boutons pendant l'animation
    (bool == true) => les boutons sont désactivés
  */
  disableButtons(bool) {
    let { styleButtonSort, styleButtonSize } = this.state;
    let listButtonSort = document.getElementsByClassName(styleButtonSort);
    let listButtonSize = document.getElementsByClassName(styleButtonSize);
    for (let i = 0; i < listButtonSort.length; i++)
      listButtonSort[i].disabled = bool;

    for (let i = 0; i < listButtonSize.length; i++)
      listButtonSize[i].disabled = bool;
  }

  /*
    Arrête les opérations en cours, réactive les boutons et génère un nouveau tableau de valeurs
  */
  newArray() {
    clearAllTimeOut(window);
    this.disableButtons(false);

    let listeValsDOM = document.getElementsByClassName("barre");
    for (let i = 0; i < listeValsDOM.length; i++) {
      listeValsDOM[i].style.backgroundColor = COLOR_DEFAULT;
    }

    this.resetArray();
  }

  /* 
    Génère un nouveau tableau
  */
  resetArray() {
    const tab = [];
    let header_height = document.getElementsByClassName("header")[0]
      .clientHeight;
    for (let i = 0; i < this.state.nb_values; i++) {
      tab.push(getRndInteger(22, window.innerHeight - header_height - 5)); //-5 car les 5px de décalage en bas
    }

    this.setState({ values: tab });
  }

  componentDidMount() {
    this.resetArray();
  }

  /*
        Teste une fonction de tri f
        Si f est correcte, aucun ERROR ne doit apparaître dans la console
    */
  test(f) {
    for (let i = 0; i < 300; i++) {
      let arr = this.state.values.slice().sort((a, b) => a - b);
      let aux_array = externMergeSort(this.state.values);

      if (arraysAreEquals(arr, aux_array)) console.log("OK");
      else console.log("ERROR");

      this.resetArray();
    }
  }

  /*
    Animations lorsque le tableau est trié
  */
  sortFinishedAnimation(listeValsDOM, count) {
    for (let i = 0; i < listeValsDOM.length; i++)
      setTimeout(() => {
        listeValsDOM[i].style.backgroundColor = COLOR_SORTED;
      }, this.state.anim_speed * count++);
  }

  /*
        @Fonction : effectue le bubbleSort sur le tableau this.state.values
        @Explications : 
            - L'ordonnancement de chaque opération se fait avec coun_t.
            - A chaque opération à effectuer sur le DOM, on augmente count_t.
            - Géré avec des setTimeOut, la première opération est programmée à T = coun_t * anim_speed = 0 * anim_speed
            - La deuxième sera programmée à T = count_t * anim_speed = 1 * anim_speed, puis à T = 2 * anim_speed, .... (coun_t est incrémenté entre chaque)

            - animations est un [][3] où chaque [3] vaut [indexI, indexJ, Comp_ou_swap] trois entiers
            - Comp_ou_swap défini par les const COMP ET SWAP plus haut
    */
  bubbleSort() {
    this.disableButtons(true);

    const { animations, aux_array } = externBubbleSort(this.state.values);
    let { anim_speed } = this.state;

    /*
        Timer décroissant
        Le temps est *2 car pour un i donné, on fait 2 actions (surligner puis remettre en COLOR_DEFAULT) 
            donc 2 fois anim_speed en temps
        Le temps total est donc +- = 2 * nb d'opérations à faire * vitesse d'une opération
    */
    startTimer((2 * (animations.length * anim_speed)) / 1000);

    const listeValsDOM = document.getElementsByClassName("barre");

    let count_t = 0; //Compteur qui va permettre d'ordonnancer l'ordre des opérations
    let i = 0;

    while (i < animations.length) {
      const trio = animations[i]; //[0],[1] => les indexes impliqués, [3] = O ou 1 selon si comp ou swap
      const [firstComparedIndex, secondComparedIndex, CompOrSwap] = trio;

      if (CompOrSwap === COMP) {
        setTimeout(() => {
          //Mise en couleur COMPARAISON
          listeValsDOM[firstComparedIndex].style.backgroundColor = COLOR_COMP;
          listeValsDOM[secondComparedIndex].style.backgroundColor = COLOR_COMP;
        }, anim_speed * count_t++);

        if (i + 1 < animations.length && animations[i + 1][2] === SWAP) {
          //Si le prochain est un swap
          setTimeout(() => {
            //Si la suite est un SWAP, alors mise en couleur SWAP
            listeValsDOM[firstComparedIndex].style.backgroundColor = COLOR_SWAP;
            listeValsDOM[
              secondComparedIndex
            ].style.backgroundColor = COLOR_SWAP;
          }, anim_speed * count_t++);

          setTimeout(() => {
            //INVERSION DES COLONNES
            let h1 = listeValsDOM[firstComparedIndex].style.height;
            listeValsDOM[firstComparedIndex].style.height =
              listeValsDOM[secondComparedIndex].style.height;
            listeValsDOM[secondComparedIndex].style.height = h1;
          }, anim_speed * count_t++);
          i++;
        }
        setTimeout(() => {
          //Qu'il y ait une inversion à la suite ou pas, on remets en DEFAULT les valeurs comparées
          listeValsDOM[
            firstComparedIndex
          ].style.backgroundColor = COLOR_DEFAULT;
          listeValsDOM[
            secondComparedIndex
          ].style.backgroundColor = COLOR_DEFAULT;
        }, anim_speed * count_t++);
      }
      i++;
    }
    /*
            A la fin, on assigne au tableau values le tableau aux_array trié
                car sinon en cliquant sur trier, on relancera un tri avec le premier tableau non modifié
                et donc on utilisera les swaps de ce premier tableau non trié, ce qui mènera à des swaps non
                adaptés à l'état actuel de la DOM avec un affichage de tableau trié (aux_array)
            (Le délai est *2 pour les mêmes raisons que + haut)
        */

    this.sortFinishedAnimation(listeValsDOM, count_t);

    setTimeout(() => {
      this.setState({ values: aux_array });
      this.disableButtons(false);
    }, anim_speed * count_t++);
  }

  /*
    @Fonction : effectue le insertionSort sur le tableau this.state.values
    @Explications :
        - Ordonnancement identique à celui de @bubbleSort()
        - animations est un [][2 ou 3]
        - si operation = animations[i] a une longueur de 2 :
            - soit c'est une nouvelle valeur à insérer [indexValeuràInserer, TypeOperation = TO_INSERT]
            - soit c'est une valeur que l'on compare à celle à insérer [indexValeurComparée, TypeOperation = COMP]
        - si il a une longueur de 3, alors c'est un swap [index1, index2, type = SWAP]

        - Le comportement global est le suivant :
            - On trouve une valeur à insérer et on la colorie
            - Tant que les opérations suivantes sont des comparaisons et des swaps, on les fait
            - On remets à COLOR_DEFAULT la valeur insérée et on passe à la prochaine à insérer
  */
  insertionSort() {
    this.disableButtons(true);

    let { anim_speed } = this.state;
    let { animations, aux_array } = externInsertionSort(this.state.values);

    let listeValsDOM = document.getElementsByClassName("barre");

    startTimer((animations.length * anim_speed) / 1000);

    let count_t = 0;
    let i = 0;

    while (i < animations.length) {
      //On parcout les opérations jusqu'à avoir une nouvelle insertion à effectuer
      let operation = animations[i];
      if (
        operation.length === 2 &&
        getOperationTypeInsertionSort(operation) === TO_INSERT
      ) {
        //On part sur une nouvelle séquence d'insertion
        let indexToModify = operation[0];
        let currentIndex = indexToModify;
        setTimeout(() => {
          listeValsDOM[indexToModify].style.backgroundColor = COLOR_INSERT;
        }, anim_speed * count_t++);

        operation = animations[++i];
        while (
          i < animations.length - 1 &&
          getOperationTypeInsertionSort(operation) !== TO_INSERT
        ) {
          //Tant que les opérations sont des comparaisons et des swaps
          if (getOperationTypeInsertionSort(operation) === SWAP) {
            let indexToModify1 = operation[0]; 
            let indexToModify2 = operation[1];
            currentIndex = indexToModify1;
            setTimeout(() => {
              let h = listeValsDOM[indexToModify1].style.height;
              listeValsDOM[indexToModify1].style.height =
                listeValsDOM[indexToModify2].style.height;
              listeValsDOM[indexToModify2].style.height = h;

              listeValsDOM[indexToModify1].style.backgroundColor = COLOR_INSERT;
              listeValsDOM[
                indexToModify2
              ].style.backgroundColor = COLOR_DEFAULT;
            }, anim_speed * count_t++);
          } else {
            let indexToModify = operation[0];
            setTimeout(() => {
              listeValsDOM[indexToModify].style.backgroundColor = COLOR_COMP;
            }, anim_speed * count_t++);
            if (
              i >= animations.length ||
              (i + 1 < animations.length &&
                getOperationTypeInsertionSort(animations[i + 1]) !== SWAP)
            )
              setTimeout(() => {
                listeValsDOM[
                  indexToModify
                ].style.backgroundColor = COLOR_DEFAULT;
              }, anim_speed * count_t++);
          }
          i++;
          operation = animations[i];
        }
        i--; //Car juste avant de sortir de la boucle, i aura été incrémenté pour rien (donc on évite une double incrémentation avec le i++ plus bas)

        setTimeout(() => {
          //Remise à COLOR_DEFAULT de la valeur à insérer après l'avoir traitée
          listeValsDOM[currentIndex].style.backgroundColor = COLOR_DEFAULT;
        }, anim_speed * count_t++);
      }
      i++;
    }

    this.sortFinishedAnimation(listeValsDOM, count_t);

    setTimeout(() => {
      this.setState({ values: aux_array });
      this.disableButtons(false);
    }, anim_speed * count_t++);
  }

  heapSort() {
    this.disableButtons(true);
    let {anim_speed, values} = this.state;

    let { animations, aux_array } = externHeapSort(values);
    let listeValsDOM = document.getElementsByClassName("barre");

    let count_t = 0, i = 0;

    while (i < animations.length){
      let operation = animations[i];
      if (getOperationTypeHeapSort(operation) === SWAP){
        let index1 = operation[0],
            index2 = operation[1];

        setTimeout(() => {
          listeValsDOM[index1].style.backgroundColor = COLOR_SWAP;
          listeValsDOM[index2].style.backgroundColor = COLOR_SWAP;
        }, anim_speed * count_t++);

        setTimeout(() => {
          let h = listeValsDOM[index1].style.height;
          listeValsDOM[index1].style.height = listeValsDOM[index2].style.height;
          listeValsDOM[index2].style.height = h; 
        }, anim_speed * count_t++);

        setTimeout(() => {
          listeValsDOM[index1].style.backgroundColor = COLOR_DEFAULT;
          listeValsDOM[index2].style.backgroundColor = COLOR_DEFAULT;
        }, anim_speed * count_t++);
      }
      /*else if (getOperationTypeHeapSort(operation) === COMP){
        let index1, index2, index3 = -1;
        if (operation.length === 3){
          index1 = operation[0];
          index2 = operation[1];
        }
        else {
          index1 = operation[0];
          index2 = operation[1];
          index3 = operation[2];
        }

        setTimeout(() => {
          listeValsDOM[index1].style.backgroundColor = COLOR_COMP;
          listeValsDOM[index2].style.backgroundColor = COLOR_COMP;
          if (index3 !== -1)
            listeValsDOM[index3].style.backgroundCOlor = COLOR_COMP;
        }, anim_speed * count_t++);
      }*/
      i++;
    }

    this.sortFinishedAnimation(listeValsDOM, count_t);
    
    setTimeout(() => {
      this.setState({
        values: aux_array
      })
      this.disableButtons(false);
    }, anim_speed * count_t);

  }

  mergeSort() {
    this.disableButtons(true);
    let {anim_speed, values} = this.state;

    let { animations, aux_array } = externMergeSort(values);
    let listeValsDOM = document.getElementsByClassName("barre");

    let count_t = 0, i = 0;
    
    /* ANIMER LES SWAPS ET LES COMPARAISON */

    this.sortFinishedAnimation(listeValsDOM, count_t);

    setTimeout(() => {
      this.setState({
        values: aux_array,
      });  
      this.disableButtons(false);
    }, anim_speed * count_t);
  }

  /*
    1) On choisit un pivot -> mise en couleur
    2) Puis, on colore l'elem de la comparaison à gauche
             on colore l'elem de la comparaison à droite
             jusqu'à swap, on swap
             (swap ou pas swap, on remets en COLOR_DEFAULT)
       TANT QUE PROCHAINE OPERATION != PIVOT 
    FIN) On passe au pivot suivant -> on remets toutes les valeurs traitées en COLOR_DEFAULT si pas déjà fait + haut
  */
  quickSort() {
    this.disableButtons(true);
    let { anim_speed } = this.state;

    let { animations, aux_array } = externQuickSort(this.state.values);
    let listeValsDOM = document.getElementsByClassName("barre");

    let count_t = 0,
      i = 0;

    let last_pivot_index = 0; //L'index du dernier pivot, utile pour le remettre à color_default
    let last_comp_gauche_index = 0;//L'index du dernier comp_gauche, utile quand le comp_droite est suivi d'un pivot, pour le remettre à color_default
    for (i = 0; i < animations.length; i++){
      let operation = animations[i];
      let typeOperation = getOperationTypeQuickSort(operation);

      if (typeOperation === PIVOT){
        let local_last_pivot_index = last_pivot_index;
        setTimeout(() => {
          listeValsDOM[local_last_pivot_index].style.backgroundColor = COLOR_DEFAULT;
        }, anim_speed * count_t++);

        setTimeout(() => {
          local_last_pivot_index = operation[0];
          listeValsDOM[local_last_pivot_index].style.backgroundColor = COLOR_PIVOT;
        }, anim_speed * count_t++);
        last_pivot_index = operation[0];
      }

      else if (typeOperation === COMP_GAUCHE){
        let index_to_modify = operation[0];
        last_comp_gauche_index = index_to_modify;
        setTimeout(() => {
          listeValsDOM[index_to_modify].style.backgroundColor = COLOR_COMP;
        }, anim_speed * count_t++);

        if (i + 1 < animations.length - 1 && getOperationTypeQuickSort(animations[i + 1]) === COMP_GAUCHE){
          setTimeout(() => {
            listeValsDOM[index_to_modify].style.backgroundColor = COLOR_DEFAULT;
          }, anim_speed * count_t++);
        }
      }

      else if (typeOperation === COMP_DROITE){
        let index_to_modify = operation[0];
        setTimeout(() => {
          listeValsDOM[index_to_modify].style.backgroundColor = COLOR_COMP;
        }, anim_speed * count_t++);

        if (i + 1 < animations.length && getOperationTypeQuickSort(animations[i + 1]) === COMP_DROITE){//Si après le comp droite il y a un autre comp_droite
          setTimeout(() => {
            listeValsDOM[index_to_modify].style.backgroundColor = COLOR_DEFAULT;
          }, anim_speed * count_t++);
        }
        else if (i + 1 < animations.length && getOperationTypeQuickSort(animations[i + 1]) === PIVOT){//Si après le comp_droite c'est un pivot, il faut remettre le comp_gauche qui est "en attente" en color_default
          let local_last_comp_gauche_index = last_comp_gauche_index;
          setTimeout(() => {
            listeValsDOM[index_to_modify].style.backgroundColor = COLOR_DEFAULT;
            listeValsDOM[local_last_comp_gauche_index].style.backgroundColor = COLOR_DEFAULT;
          }, anim_speed * count_t++);
        }
      }

      else{//donc SWAP
        let index1 = operation[0];
        let index2 = operation[1];

        if (last_pivot_index === index1 || last_pivot_index === index2)//Changement de pos du dernier pivot dans le cas où le pivot est impliqué dans le swap
          last_pivot_index = (last_pivot_index===index1)?index2:index1;

        let local_last_pivot_index = last_pivot_index;

        setTimeout(() => {
          listeValsDOM[index1].style.backgroundColor = COLOR_SWAP;
          listeValsDOM[index2].style.backgroundColor = COLOR_SWAP;
        }, anim_speed * count_t++);

        setTimeout(() => {
          let h = listeValsDOM[index1].style.height;
          listeValsDOM[index1].style.height = listeValsDOM[index2].style.height;
          listeValsDOM[index2].style.height = h;
        }, anim_speed * count_t++);

        setTimeout(() => {
          listeValsDOM[index1].style.backgroundColor = COLOR_DEFAULT;
          listeValsDOM[index2].style.backgroundColor = COLOR_DEFAULT;
          listeValsDOM[local_last_pivot_index].style.backgroundColor = COLOR_PIVOT;
        }, anim_speed * count_t++);
      }
    }

    setTimeout(() => {//On remets à color_default le dernier pivot
      listeValsDOM[last_pivot_index].style.backgroundColor = COLOR_DEFAULT;
    }, anim_speed * count_t++);

    this.sortFinishedAnimation(listeValsDOM, count_t);

    setTimeout(() => {
      this.setState({
        values: aux_array,
      });
      this.disableButtons(false);
    }, anim_speed * count_t);
  }

  /*
        @Fonction : Calcule le style(largeur) des divs "barre" en fonction du nombre d'éléments
        @param    : la valeur de chaque div
        @return   : un style CSS pour une div "barre"
    */
  getDIVStyle(valeur) {
    return {
      height: `${valeur}px`,
      backgroundColor: COLOR_DEFAULT,
      width: this.state.value_width,
    };
  }

  /*
    Change le nombre de valeurs à afficher(et dans le tableau), et donc la vitesse d'animation et la largeur de chaque valeur
  */
  setArraySize(newSize) {
    this.state.nb_values = newSize;
    this.state.anim_speed =
      newSize === NB_VALUES_PETIT
        ? ANIM_SPEED_PETIT
        : newSize === NB_VALUES_MOYEN
        ? ANIM_SPEED_MOYEN
        : ANIM_SPEED_GRAND;
    this.state.value_width = window.innerWidth / (2 * this.state.nb_values);

    let listeValsDOM = document.getElementsByClassName("barre");

    for (let i = 0; i < listeValsDOM.length; i++)
      listeValsDOM[i].style.backgroundColor = COLOR_DEFAULT;

    this.resetArray();
  }

  render() {
    const { values } = this.state;

    return (
      <>
        <div className="header">
          <table>
            <tbody>
              <tr>
                <td>
                  <button
                    className={this.state.styleButtonNewArray}
                    onClick={() => {
                      this.newArray();
                    }}
                  >
                    {" "}
                    Nouvelles Valeurs Aléatoires{" "}
                  </button>
                </td>
                <td>
                  <button
                    className={this.state.styleButtonSort}
                    onClick={() => {
                      this.bubbleSort();
                    }}
                  >
                    {" "}
                    Tri à Bulles : O(n²){" "}
                  </button>
                  <button
                    className={this.state.styleButtonSort}
                    onClick={() => {
                      this.insertionSort();
                    }}
                  >
                    {" "}
                    Tri par Insertion : O(n²){" "}
                  </button>
                  <button
                    className={this.state.styleButtonSort}
                    onClick={() => {
                      this.heapSort();
                    }}
                  >
                    {" "}
                    Tri par Tas : O(n.log(n)){" "}
                  </button>
                  <button
                    className={this.state.styleButtonSort}
                    onClick={() => {
                      this.mergeSort();
                    }}
                  >
                    {" "}
                    Tri Fusion : O(n.log(n)){" "}
                  </button>
                  <button
                    className={this.state.styleButtonSort}
                    onClick={() => {
                      this.quickSort();
                    }}
                  >
                    {" "}
                    Tri Rapide : O(n.log(n)){" "}
                  </button>
                </td>
                <td>
                  <button
                    className={this.state.styleButtonSize}
                    onClick={() => {
                      this.setArraySize(NB_VALUES_PETIT);
                    }}
                  >
                    {" "}
                    Petit Jeu de Données{" "}
                  </button>
                  <button
                    className={this.state.styleButtonSize}
                    onClick={() => {
                      this.setArraySize(NB_VALUES_MOYEN);
                    }}
                  >
                    {" "}
                    Grand Jeu de Données{" "}
                  </button>
                  <button
                    className={this.state.styleButtonSize}
                    onClick={() => {
                      this.setArraySize(NB_VALUES_GRAND);
                    }}
                  >
                    {" "}
                    Très Grand Jeu de Données{" "}
                  </button>
                </td>
                <td>
                  <div>
                    {" "}
                    Temps restant : <span id="time"> 00:00 </span>{" "}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className="barres-wrapper">
          {values.map((valeur, index) => (
            <div className="barre" key={index} style={this.getDIVStyle(valeur)}>
              {<span></span>}
            </div>
          ))}
        </div>
      </>
    );
  }
}

export default SortingVisualizer;
