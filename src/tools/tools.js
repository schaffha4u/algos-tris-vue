export function startTimer(duration) {
  let display = document.getElementById("time");
  var timer = duration,
    minutes,
    seconds;
  let clearIntervalId = setInterval(function () {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    display.textContent = minutes + ":" + seconds;

    if (--timer < 0) {
      clearInterval(clearIntervalId);
    }
  }, 1000);
}

export function arraysAreEquals(a, b) {
  if (a.length !== b.length) return false;
  else {
    for (let i = 0; i < a.length; i++) if (a[i] !== b[i]) return false;
  }
  return true;
}

/*
Désactive les boutons pendant l'animation
  (bool == true) => les boutons sont désactivés
*/
export function disableButtons(bool) {
  let listButtons = document.getElementsByClassName(
    this.state.styleButtonHeader
  );
  for (let i = 0; i < listButtons.length; i++) {
    listButtons[i].disabled = bool;
  }
}

/*
    https://stackoverflow.com/questions/8860188/javascript-clear-all-timeouts#:~:text=To%20clear%20all%20timeouts%20they,)%20timeouts%20(Gist%20link).
    Stoppe tous les setTimeOut lancés
  */
export function clearAllTimeOut(windowObject) {
  var id = Math.max(
    windowObject.setTimeout(noop, 1000)
  );

  while (id--) {
    windowObject.clearTimeout(id);
  }

  function noop() {}
}

/*
        @Fonction : get random Integer  
        @return : random integer between min & max included
        https://www.w3schools.com/js/js_random.asp
    */
export function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
