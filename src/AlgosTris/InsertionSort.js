const COMP = 0;
const SWAP = 1;
const TO_INSERT = 2;

export function externInsertionSort(array) {
  let aux_array = array.slice(0);
  let animations = [];

  let toInsert;

  for (let i = 1; i < aux_array.length; i++) {
    toInsert = aux_array[i];

    animations.push([i, TO_INSERT]);
    //colorer toInsert car il est à insérer

    let j = i - 1;

    animations.push([j, COMP]);
    //Première comparaison avec le dernier elemn de l'emsemble ds lequel on insère
    while (j >= 0 && aux_array[j] > toInsert) {
      aux_array[j + 1] = aux_array[j];
      animations.push([j, j + 1, SWAP]);
      /*
        Swap j + 1 et j
      */
      j--;
      if (j >= 0) animations.push([j, COMP]);
      /*
        Si on doit insérer, alors on mets à jour la comparaison avec le bon index
        On compare ici, comme ça on peut comparer avec le premier elem non valide pour échange 
            (qui fera sortir de la boucle)
        Donc, ici comparer comme si la boucle était dans l'itération suivante
      */
    }

    aux_array[j + 1] = toInsert;
  }

  return { animations, aux_array };
}

export function getOperationTypeInsertionSort(arr) {
  return arr.length === 2 ? arr[1] : arr[2];
}
