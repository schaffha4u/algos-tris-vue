const COMP = 0;
const SWAP = 1;
const MILLIEU = 2;

export function externMergeSort(array){
    let animations = [];
    let aux_array = array.slice(0);

    mergesort(aux_array, 0, aux_array.length, animations);

    return { animations, aux_array };
}

function mergesort(a, lo, hi, animations) {
    if (hi - lo > 1) {
        var m = lo + ((hi - lo) >> 1);
        mergesort(a, lo, m, animations);
        mergesort(a, m, hi, animations);
        merge(a, lo, m, hi, animations);
    }
}

function swap(arr, a, b){
    let temp = arr[a];
    arr[a] = arr[b];
    arr[b] = temp;
}

function merge(a, lo, m, hi) {
    var tmp = [];
    var len = m - lo;
    var i, j, k;
    // save left subarray
    for (i = 0; i < len; i++) {
        tmp[i] = a[lo + i];
    }
    // merge subarrays
    i = 0;
    j = m;
    k = lo;
    while (i < len && j < hi) {
        if (tmp[i] <= a[j]) {
            // animate this move
            a[k++] = tmp[i++];
        } else {
            // animate this move
            a[k++] = a[j++];
        }
    }
    // copy the remaining elements
    while (i < len) {
        // animate this move
        a[k++] = tmp[i++];
    }
}