/*
    COMPARAISON GAUCHE -----
    COMPARAISON DROITE -----
    SWAP (chanement de coloration, swap, remise en coloration normale) -----
    CHOIX DU PIVOT A METTRE EN SURBRILLANCE --------
    ENTREE PARTITION --------
    SORTIE PARTITION
*/
const COMP_GAUCHE = -1; //Comparaison à gauche du pivot
const COMP_DROITE = 0; //..............droite du pivot
const SWAP = 1;
const PIVOT = 2;

export function externQuickSort(arr){
    var animations = [];
    var aux_array = [];
    aux_array = externQuickSort1(arr.slice(0), 0, arr.length - 1, animations);
    return {animations, aux_array};
}

function externQuickSort1(items, left, right, animations){
    let index;
    
    if (items.length > 1){
        index = partition(items, left, right, animations);

        if (left < index - 1)
            externQuickSort1(items, left, index - 1, animations);
        if (index < right)
            externQuickSort1(items, index, right, animations);
    }
    return items;
}

function partition(items, left, right, animations){
    let indexPivot = Math.floor((right + left) / 2),
        pivot = items[indexPivot],
        i = left,
        j = right;

    animations.push([indexPivot, PIVOT]);
    while (i <= j){
        animations.push([i, COMP_GAUCHE]);
        while (items[i] < pivot){
            i++;
            animations.push([i, COMP_GAUCHE]);
        }

        animations.push([j, COMP_DROITE]);
        while (items[j] > pivot){
            j--;
            animations.push([j, COMP_DROITE]);
        }
        
        if (i <= j){
            swap(items, i, j);
            animations.push([i, j, SWAP]);
            i++;
            j--;
        }
    }
    return i;
}

/*
    Echange 2 valeurs dans arr
*/
function swap(arr, i, j){
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

export function getOperationTypeQuickSort(operation){
    return (operation.length === 2 ? operation[1]: operation[2]);
}

