const COMP = 0;
const SWAP = 1;

export function getOperationTypeHeapSort(operation){
    if (operation.length === 3) return operation[2];
    if (operation.length === 4) return operation[3];
}

export function externHeapSort(array) {
    var aux_array = array.slice(0);
    let animations = [];
    let size = aux_array.length;
  
    // build heapSort (rearrange array)
    for (let i = Math.floor(size / 2 - 1); i >= 0; i--)
      heapify(aux_array, size, i, animations)
  
    // one by one extract an element from heapSort
    for (let i = size - 1; i >= 0; i--) {
      // move current root to end
      animations.push([0, i, SWAP]);
      let temp = aux_array[0]
      aux_array[0] = aux_array[i]
      aux_array[i] = temp
  
      // call max heapify on the reduced heapSort
      heapify(aux_array, i, 0, animations)
    }

    return { animations, aux_array};
  }
  
  // to heapify a subtree rooted with node i which is an index in array[]
  function heapify(array, size, i, animations) {
    let max = i // initialize max as root
    let left = 2 * i + 1
    let right = 2 * i + 2
    
    if (left < size && right < size)
        animations.push([max, left, right, COMP]);
    else if (left < size)
        animations.push([max, left, COMP]);

    // if left child is larger than root
    if (left < size && array[left] > array[max])
      max = left
  
    // if right child is larger than max
    if (right < size && array[right] > array[max])
      max = right
  
    // if max is not root
    if (max !== i) {
      // swap
      animations.push([max, i, SWAP]);
      let temp = array[i]
      array[i] = array[max]
      array[max] = temp
  
      // recursively heapify the affected sub-tree
      heapify(array, size, max, animations)
    }
  }