const COMP = 0;
const SWAP = 1;

/*
    @Fonction        : Effectue le bubbleSort dans un tableau auxiliaire
    @param array     : Le tableau de base, les valeurs à trier
    @param aux_array : vaut array à l'appel, permets de réaliser le tri sans modifier array,
                        qui détermine l'affichage dans le DOM
    @Return Value    : Un tableau d'opérations à réaliser (comparaisons et swaps), et le tableau auxiliaire trié
*/
function externBubbleSort(array) {
  let aux_array = array.slice(0);
  let animations = [];

  for (let i = 0; i < aux_array.length; i++) {
    let sorted = true;
    for (let j = 0; j < aux_array.length - i - 1; j++) {
      animations.push([j, j + 1, COMP]);
      if (aux_array[j] > aux_array[j + 1]) {
        animations.push([j, j + 1, SWAP]);
        swap(aux_array, j, j + 1);
        sorted = false;
      }
    }
    if (sorted) break;
  }

  return {
    animations: animations,
    aux_array: aux_array,
  };
}

function swap(arr, a, b) {
  let temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}

export default externBubbleSort;
