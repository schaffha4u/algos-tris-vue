# Bienvenue !

## Bienvenue à toi, qui arrive sur ce dépôt.

Etant étudiant en Licence 3 Informatique, je m'intéresse particulièrement à l'algorithmique, l'analyse d'algorithmes et le développement de logiciels.
Te voici ici sur mon projet perso : un site permettant de visualiser le comportement de différents algorithmes de tri (comment ils manipulent les données).

**Comment faire?** Il te suffit de te rendre au lien suivant : **[Algos de Tris - Visualizer](https://sad-volhard-f4f184.netlify.app/)**.
Ensuite, tu n'as qu'à simplement cliquer sur l'algorithme de ton choix, puis laisser la magie opérer !

La technologie utilisée est [React JS](https://fr.reactjs.org/).

Les algorithmes que je souhaite implémenter sont les suivants : 
- [X] Tri à Bulles      : O(n²)
- [X] Tri par Insertion : O(n²)
- [ ] Tri par Tas       : O(n.log(n))
- [ ] Tri Fusion        : O(n.log(n))
- [X] Tri Rapide        : O(n.log(n))

Ce projet m'a été inspiré par le [projet](https://github.com/clementmihailescu/Sorting-Visualizer) de Clément Mihailescu.

## Installation (en local)

Prérequis : NodeJS

```
git clone https://gitlab.com/schaffha4u/algos-tris-vue.git
cd algos-trie-vue
npm install
npm start
```
